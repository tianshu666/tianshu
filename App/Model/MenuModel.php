<?php
declare(strict_types=1);
namespace App\Model;


class MenuModel extends BaseModel
{
    protected $tableName = 'Menu';
    
    /**
     *
     * @param int    $page     1
     * @param int    $pageSize 10
     * @param string $field    *
     *
     * @return array[total,list]
     * @throws \EasySwoole\ORM\Exception\Exception
     * @throws \Throwable
     */
    public function getList(int $page = 1, int $pageSize = 10, string $field = '*'): array
    {
        $list = $this
            ->withTotalCount()
            ->order($this->schemaInfo()->getPkFiledName(), 'DESC')
            ->field($field)
            ->limit($pageSize * ($page - 1), $pageSize)
            ->all();
        $total = $this->lastQueryResult()->getTotalCount();;
        return ['total' => $total, 'list' => $list];
    }
}