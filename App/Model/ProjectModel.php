<?php
declare(strict_types=1);

namespace App\Model;


use App\Model\BaseModel;

class ProjectModel extends BaseModel
{
    protected $tableName = 'project';
    
    /**
     * $value mixed 是原值
     */
    protected function setDbconfAttr($value)
    {
        return json_encode($value);
    }
    protected function getDbconfAttr($value){
        return json_decode($value, true);
    }
    /**
     *
     * @param int    $page     1
     * @param int    $pageSize 10
     * @param string $field    *
     *
     * @return array[total,list]
     * @throws \EasySwoole\ORM\Exception\Exception
     * @throws \Throwable
     */
    public function getList(int $page = 1, int $pageSize = 10, string $field = '*'): array
    {
        $list = $this
            ->withTotalCount()
            ->order($this->schemaInfo()->getPkFiledName(), 'DESC')
            ->field($field)
            ->limit($pageSize * ($page - 1), $pageSize)
            ->all();
        $total = $this->lastQueryResult()->getTotalCount();;
        return ['total' => $total, 'list' => $list];
    }
    
}