<?php
declare(strict_types=1);

namespace App\Model;

use App\Model\BaseModel;

class ComponentModel extends BaseModel
{
    protected $tableName = 'component';
    
    protected function setTplAttr($value){
        return json_encode($value);
    }
    /**
     * @param int    $page
     * @param int    $pageSize
     * @param string $field
     *
     * @return array
     * @throws \EasySwoole\ORM\Exception\Exception
     * @throws \Throwable
     */
    function getList(int $page = 1, int $pageSize = 10, string $field = '*'): array
    {
        $list = $this
            ->withTotalCount()
            ->order($this->schemaInfo()->getPkFiledName(), 'DESC')
            ->field($field)
            ->limit($pageSize * ($page - 1), $pageSize)
            ->all();
        $total = $this->lastQueryResult()->getTotalCount();;
        return ['total' => $total, 'list' => $list];
    }
}