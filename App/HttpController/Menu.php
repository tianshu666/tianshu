<?php
declare(strict_types=1);

namespace App\HttpController;

use App\Model\MenuModel;
use EasySwoole\Http\Message\Status;

class Menu extends Base
{
    /**
     * 菜单列表
     * @throws \EasySwoole\ORM\Exception\Exception
     * @throws \Throwable
     */
    function index()
    {
        $param = $this->request()->getRequestParam();
        $page = (int)($param['page']??1);
        $limit = (int)($param['limit']??20);
        $model = new MenuModel();
        $data = $model->getList($page,  $limit);
        $this->writeJson(Status::CODE_OK, $data, 'success');
    }
    
    /**
     * 创建菜单
     * @throws \EasySwoole\ORM\Exception\Exception
     * @throws \Throwable
     */
    function create(){
        $param = $this->request()->getRequestParam();
        $model = new MenuModel($param);
        $rs = $model->save();
        if ($rs) {
            $this->writeJson(Status::CODE_OK, $model->toArray(), "success");
        } else {
            $this->writeJson(Status::CODE_BAD_REQUEST, [], $model->lastQueryResult()->getLastError());
        }
    }
    
    /**
     * 更新菜单
     *
     * @param $id
     *
     * @return bool
     * @throws \EasySwoole\Mysqli\Exception\Exception
     * @throws \EasySwoole\ORM\Exception\Exception
     * @throws \Throwable
     */
    function update($id){
        $param = $this->request()->getRequestParam();
        $model = new MenuModel();
        $info = $model->get($id);
        if (empty($info)) {
            $this->writeJson(Status::CODE_BAD_REQUEST, [], '该数据不存在');
            return false;
        }
        $updateData = [];
        
        $updateData['id'] = $info->id;
        $updateData['name'] = $param['name']??$info->name;
        $updateData['version'] = $param['version']??$info->version;
        $updateData['tpl'] = $param['tpl']??$info->tpl;
        $updateData['type'] = $param['type']??$info->type;
        $updateData['status'] = $param['status']??$info->status;
        $rs = $info->update($updateData);
        if ($rs) {
            $this->writeJson(Status::CODE_OK, $rs, "success");
        } else {
            $this->writeJson(Status::CODE_BAD_REQUEST, [], $model->lastQueryResult()->getLastError());
        }
    }
    
    /**
     * 读取组件
     *
     * @param $id
     *
     * @throws \EasySwoole\Mysqli\Exception\Exception
     * @throws \EasySwoole\ORM\Exception\Exception
     * @throws \Throwable
     */
    function read($id){
        $param = $this->request()->getRequestParam();
        $model = new MenuModel();
        $rs  = $model->get(['id' => $id]);
        if ($rs) {
            $this->writeJson(Status::CODE_OK, $rs, "success");
        } else {
            $this->writeJson(Status::CODE_BAD_REQUEST, [], 'fail');
        }
    }
    
    /**
     * 删除菜单
     *
     * @param $id
     *
     * @throws \EasySwoole\ORM\Exception\Exception
     * @throws \Throwable
     */
    function delete($id){
        $param = $this->request()->getRequestParam();
        $model = new MenuModel();
        
        $rs = $model->destroy($id);
        if ($rs) {
            $this->writeJson(Status::CODE_OK, [], "success");
        } else {
            $this->writeJson(Status::CODE_BAD_REQUEST, [], 'fail');
        }
    }
    
}