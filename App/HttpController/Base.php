<?php

declare(strict_types=1);
namespace App\HttpController;
use EasySwoole\ORM\Db\Config;
use EasySwoole\FastCache\Cache;
abstract class Base  extends \EasySwoole\Http\AbstractInterface\Controller
{
    public function index()
    {
        $this->actionNotFound('index');
    }
    public function onRequest(?string $action): ?bool
    {
        if (!parent::onRequest($action)) {
            return false;
        };
        return true;
    }
    //返回项目数据库
    public function getProjectDb(): object
    {
        //获取项目配置
        $project = Cache::getInstance()->get('project');
        $dpconf = $project['dbconf'];
        $config = new Config();
        $config->setDatabase(trim($dpconf['database']));
        $config->setUser(trim($dpconf['username']));
        $config->setPassword(trim($dpconf['password']));
        $config->setHost(trim($dpconf['host']));
        //连接池配置
        $config->setGetObjectTimeout(3.0); //设置获取连接池对象超时时间
        $config->setIntervalCheckTime(30*1000); //设置检测连接存活执行回收和创建的周期
        $config->setMaxIdleTime(15); //连接池对象最大闲置时间(秒)
        $config->setMaxObjectNum(20); //设置最大连接池存在连接对象数量
        $config->setMinObjectNum(5); //设置最小连接池存在连接对象数量
        return $config;
    }
    
}