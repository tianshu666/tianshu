<?php
declare(strict_types=1);
namespace App\HttpController;
use App\HttpController\Base;
use EasySwoole\Utility\File;
use EasySwoole\Utility\Str;
use EasySwoole\Http\Message\Status;

use EasySwoole\Mysqli\QueryBuilder;
use EasySwoole\ORM\DbManager;
use EasySwoole\ORM\Db\Connection;
use EasySwoole\ORM\Db\Config;

/**
 * Class Sql
 *
 * @package App\HttpController
 * @author  : jingzhou
 * @email   : xunzhou@leubao.com
 * @date    : 2020/2/17 01:07
 * @desc    : 数据库操作
 */
class Sql extends Base
{
    //获取数据库表结构
    //获取数据字典
    //获取指定数据表结构
    
    //数据全部数据表
    public function getAllTable()
    {
        $queryBuild = new QueryBuilder();
        // 支持参数绑定 第二个参数非必传
        $queryBuild->raw("SHOW TABLE STATUS");
        $data = DbManager::getInstance()->query($queryBuild, true, 'default');
        $tables = array();
        foreach ($data->getResult() as $k => $v) {
            $name = '';
            $name = Str::studly($v['Name']);
            $extend = [
                'c_path'		=>	"App\HttpController",
                'm_path'		=>	"App\Model\\".$name,
                'b_path'		=>	"App\Model\\".$name,
                'v_path'		=>	"App\Validator",
                'l_path'		=>	"App\Logic",
            ];
            $table[] = [
                'name'			=>	$v['Name'],
                'extend'		=>	json_encode($extend),
                'title'			=>	$v['Comment'],
            ];
        }
        $return = [
            'table'	=>	$table,
            'dir'	=>	$this->getDirList(),
        ];
        $this->writeJson(Status::CODE_OK, $return, '数据表');
    }
    //获取目录
    public function getDirList()
    {
        $path = '/www/wwwroot/http/App/HttpController/';
        $dir = scandir($path);
        foreach ($dir as $key => $value) {
            if(is_dir($path.$value) && substr($value, 0, 1) != '.'){
                $dirc[] = $value;
            }
        }
        return $dirc;
    }
    //获取数据字典
    public function ddic()
    {
        $tables = array();
        $queryBuild = new QueryBuilder();
        // 支持参数绑定 第二个参数非必传
        $queryBuild->raw("SHOW TABLE STATUS");
        $data = DbManager::getInstance()->query($queryBuild, true, 'default');
        foreach ($data->getResult() as $k => $v) {
            $site[] = [
                'name'			=>	$v['Name'],
                'title'			=>	$v['Comment'],
            ];
            $tables[] = [
                'name'			=>	$v['Name'],
                'title'			=>	$v['Comment'],
                'engine'		=>	$v['Engine'],
                'field'			=>	$db->rawQuery("show full columns from {$v['Name']}")
            ];
        }
        $return = [
            'site'	=>	$site,
            'tables'=>	$tables
        ];
        $this->writeJson(Status::CODE_OK, $return, '数据字典');
    }
    //数据库链接测试
    function getSqlLink(){
        $projectName = 'test';
        try{
            $param = $this->request()->getRequestParam();
            $config = new Config();
            $config->setDatabase(trim($param['database']));
            $config->setUser(trim($param['username']));
            $config->setPassword(trim($param['password']));
            $config->setHost(trim($param['host']));
            //连接池配置
            $config->setGetObjectTimeout(3.0); //设置获取连接池对象超时时间
            $config->setIntervalCheckTime(30*1000); //设置检测连接存活执行回收和创建的周期
            $config->setMaxIdleTime(15); //连接池对象最大闲置时间(秒)
            $config->setMaxObjectNum(20); //设置最大连接池存在连接对象数量
            $config->setMinObjectNum(5); //设置最小连接池存在连接对象数量
    
            DbManager::getInstance()->addConnection(new Connection($config), $projectName);
    
            $queryBuild = new QueryBuilder();
            // 支持参数绑定 第二个参数非必传
            $queryBuild->raw("SHOW TABLE STATUS");
            $data = DbManager::getInstance()->query($queryBuild, true, $projectName);
            $this->writeJson(Status::CODE_OK, [], '链接成功~');
        } catch (\Exception $e) {
            $this->writeJson(Status::CODE_SEE_OTHER, [], '数据库链接失败');
        }
    }
    function index()
    {
        // TODO: Implement index() method.
    }
}