<?php
declare(strict_types=1);

namespace App\HttpController;


use App\Logic\ProjectInit;
use App\Model\ProjectModel;
use EasySwoole\Http\Message\Status;
use EasySwoole\Utility\File;

/**
 * Class Project
 *
 * @package App\HttpController
 * @author  : jingzhou
 * @email   : xunzhou@leubao.com
 * @date    : 2020/2/17 00:48
 * @desc    : 项目管理
 */
class Project extends Base
{
    //项目列表
    function index()
    {
        $param = $this->request()->getRequestParam();
        $page = (int)($param['page']??1);
        $limit = (int)($param['limit']??20);
        $model = new ProjectModel();
        $data = $model->getList($page,  $limit);
        $this->writeJson(Status::CODE_OK, $data, 'success');
    }
    //创建项目
    function create(){
        $param = $this->request()->getRequestParam();
        $model = new ProjectModel($param);
        $rs = $model->save();
        if ($rs) {
            //初始化项目
            $logic = new ProjectInit();
            $logic->initBaseProject();
        
            $this->writeJson(Status::CODE_OK, $model->toArray(), "success");
        } else {
            $this->writeJson(Status::CODE_BAD_REQUEST, [], $model->lastQueryResult()->getLastError());
        }
    }
    //项目删除
    function delete(){
    
    }
    //项目发布
    function projectRelease(){
        $version = date('Ymd');//发布版本
        //按功能构建 server 服务端   admin 后台管理页面  program小程序
        $projectName = 'test';
        $projectDir = '/Library/WebServer/Documents/ea-xzusoft/Project/'.$projectName.'/dist/'.$version;
        $releaseDir = [
            'program',
            'server',
            'admin'
        ];
        foreach ($releaseDir as $v){
            $path = $projectDir.'/'.$v;
            File::createDirectory($path);
        }
        //拷贝移动文件
        $this->writeJson('200', [], '项目发布成功~');
    }
}