<?php

declare(strict_types=1);
namespace App\HttpController;
use App\HttpController\Base;
use EasySwoole\Utility\File;

/**
 * Class Filevue
 *
 * @package App\HttpController
 * @author  : jingzhou
 * @email   : xunzhou@leubao.com
 * @date    : 2020/2/17 00:51
 * @desc    : 前端文件处理
 */
class Filevue extends Base
{
    
    function index()
    {
        // TODO: Implement index() method.
    }
    //获取组件库
    function getComponentList(){
    
    }
    //根据模板创建前端文件
    function setTplCreateVue(){
        //读取模板
        //整合变量
        //生成文件
        $projectName = 'test';
        $projectDir = '/Library/WebServer/Documents/ea-xzusoft/Project/'.$projectName.'/Public/';
        $fileName = 'list';
        $filePath = $projectDir.$fileName.'.vue';
        $content = '12121';
        File::createFile($filePath, $content);
        
        $this->writeJson('200', [], $fileName.'创建成功~');
    }
}