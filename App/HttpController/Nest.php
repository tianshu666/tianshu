<?php
declare(strict_types=1);

namespace App\HttpController;
use App\HttpController\Base;
use EasySwoole\Utility\File;
use App\Model\ProjectModel;
use EasySwoole\Http\Message\Status;
use App\Logic\ProjectInit;
use EasySwoole\FastCache\Cache;
class Nest extends Base
{
    function index()
    {
        //获取当前项目根路径
        $projectName = 'test';
        $projectDir = TIANSHU_ROOT.$projectName.'/Public/';
        //移动文件夹测试
        $source = '/Library/WebServer/Documents/xz-vue-elm-admin/src';
        File::copyDirectory($source, $projectDir);

        //移动必要文件 不成功
        $sourceFile = '/Library/WebServer/Documents/ea-xzusoft/composer.json';
        File::copyFile($sourceFile, $projectDir);
        
        $this->writeJson('200', ['1' => dirname(__FILE__), __DIR__,EASYSWOOLE_ROOT,TIANSHU_ROOT], '初始化成功~');
    }
    //项目列表
    function project(){
        $param = $this->request()->getRequestParam();
        $page = (int)($param['page']??1);
        $limit = (int)($param['limit']??20);
        $model = new ProjectModel();
        $data = $model->getAll($page,  $limit);
        $this->writeJson(Status::CODE_OK, $data, 'success');
    }
    //系统默认配置
    function getProjectInit(){
        $default = [
            'path' => '/Library/WebServer/Documents/ea-xzusoft/Project/',
            
        
        ];
    }
    //创建项目
    function createProject(){
        $param = $this->request()->getRequestParam();
        $model = new ProjectModel($param);
        $rs = $model->save();
        if ($rs) {
            //初始化项目
            $logic = new ProjectInit();
            $logic->initBaseProject();
            
            $this->writeJson(Status::CODE_OK, $model->toArray(), "success");
        } else {
            $this->writeJson(Status::CODE_BAD_REQUEST, [], $model->lastQueryResult()->getLastError());
        }
    }
    //设置当前操作项目
    function selectedProject($id){
        $param = $this->request()->getRequestParam();
        
        $model = new ProjectModel();
        $rs  = $model->get($param['id']);
        if(!$rs){
            $this->writeJson(Status::CODE_BAD_REQUEST, [], '项目不存在');
        }
        //设置当前项目缓存
        Cache::getInstance()->set('project', $rs);
        
    }
}