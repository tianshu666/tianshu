<?php

declare(strict_types=1);
namespace App\HttpController;

use EasySwoole\Http\AbstractInterface\AbstractRouter;
use FastRoute\RouteCollector;
use EasySwoole\Http\Request;
use EasySwoole\Http\Response;

class Router extends AbstractRouter
{
    function initialize(RouteCollector $routeCollector)
    {
        $this->setGlobalMode(true);
        
        $routeCollector->get('/', '/Index');
        //项目
        $routeCollector->addGroup('/project', function (RouteCollector $route){
            $route->get('/index', 'Project/Project/index');
            $route->post('/create', 'Project/Project/create');
            
            $route->post('/aiphpcode', 'Project/Filephp/autocurd');
            //生成前端文件
            $route->post('/aivuecode', 'Project/Filevue/setTplCreateVue');
        });
        //数据库
        $routeCollector->addGroup('/sql', function (RouteCollector $route){
            $route->post('/linksql', 'Project/Sql/getSqlLink');
            $route->get('/table', 'Project/Sql/getAllTable');
            $route->get('/ddic', 'Project/Sql/projectRelease');
        });
        //文件
        $routeCollector->addGroup('/project', function (RouteCollector $route){
            $route->post('/aiphpcode', 'Project/Filephp/autocurd');
            //生成前端文件
            $route->post('/aivuecode', 'Project/Filevue/setTplCreateVue');
        });
        //组件
        $routeCollector->addGroup('/cmp', function (RouteCollector $route){
            $route->get('/index', 'Project/Project/index');
            $route->post('/create', 'Project/Project/create');
        });
        $routeCollector->addGroup('/nest', function (RouteCollector $router){
            //其它
            $router->get('/index', 'Project/Project/index');
            $router->get('/init', 'Project/Project/initProject');
            $router->get('/tplvue', 'Project/Project/setTplCreateVue');
            $router->get('/release', 'Project/Project/projectRelease');
            
        });
    
        $this->setMethodNotAllowCallBack(function (Request $request, Response $response){
            $response->write('未找到处理方法');
            return false;//结束此次响应
        });
        $this->setRouterNotFoundCallBack(function (Request $request, Response $response){
            $response->write('未找到路由匹配');
            return 'index';//重定向到index路由
        });
    }
}