<?php
declare(strict_types=1);

namespace App\Logic;


use EasySwoole\Utility\File;

class ProjectInit extends Logic
{
    private $projectName;
    
    public function __construct(string $name){
        $this->projectName = $name;
    }
    //初始化项目
    function initBaseProject(){
        $initDir = [
            'App',
            'App/HttpController',
            'App/Model',
            'App/Logic',
            'App/Validator',
            'App/Utils',
            'Public',//前端后端
            'Uniapp',//小程序
            'dist',//项目发布目录
        ];
        //构建项目目录
        $projectDir = TIANSHU_ROOT.$this->projectName.'/';
        foreach ($initDir as $v){
            $path = $projectDir.$v;
            File::createDirectory($path);
        }
        //拷贝必要文件
        $source = '/Library/WebServer/Documents/ea-xzusoft/composer.json';
        $source = '/Library/WebServer/Documents/ea-xzusoft/InitFile/Router.php';
        File::copyFile($source, $projectDir);
        return [];
    }
}