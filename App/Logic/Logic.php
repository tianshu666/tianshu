<?php
declare(strict_types=1);
namespace App\Logic;

abstract class Logic
{
    public $error = 0;
    
    public function setError(string $msg)
    {
        $this->error = $msg;
    }
    
    public function getError()
    {
        return $this->error;
    }
}