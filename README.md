![](tianshu.png)

## 天枢自动编码
基于世界上最好的语言PHP构建，高效完成你的项目
## 特性
- 支持[ThinkPHP6](https://gitee.com/liu21st/thinkphp) 与 [EasySwoole](https://gitee.com/easyswoole/easyswoole)框架
- 基于数据库表结构快速构建高可用项目
- 一次构建同步生成前后端分离代码
## 目录结构
project                   项目部署目录  
├─App                     应用目录(可以有多个)   
│  ├─HttpController       控制器目录  
│  │  └─Index.php         默认控制器  
│  ├─Logic 				  逻辑层  
│  └─Model                模型文件目录  
├─Log                     日志文件目录  
├─Temp                    临时文件目录  
├─vendor                  第三方类库目录  
├──Template 项目基础模板   
│   ├── EasySwoole  	  基于EasySwoole项目基础模板  
│   └── Thinkphp6         基于ThinkPHP6 项目基础模板  
├─composer.json           Composer架构  
├─composer.lock           Composer锁定  
├─EasySwooleEvent.php     框架全局事件  
├─easyswoole              框架管理脚本  
├─dev.php                 开发配置文件  
├─produce.php             生产配置文件  
## 文档
